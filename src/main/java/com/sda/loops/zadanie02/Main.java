package com.sda.loops.zadanie02;

import com.sda.Utils;

/*
Napisz program, który wyświetla n liczb naturalnych, gdzie ilość liczb wprowadza
użytkownik za pomocą konsoli
 */
public class Main {
    public static void main(String[] args) {
        int iloscLiczb = Utils.pobierzLiczbeZKonsoli();
        for (int i = 1; i < iloscLiczb + 1; i++) {
            System.out.print(i + " ");
        }
    }
}
