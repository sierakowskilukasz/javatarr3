package com.sda.loops.zadanie01;

/*
Napisz program, który wyświetla 10 liczb naturalnych.
1 2 3 4 5 6 7 8 9 10
 */
public class Main {
    public static void main(String[] args) {
        for (int i = 1; i < 11; i++) {
            System.out.print(i + " ");
        }
    }
}
