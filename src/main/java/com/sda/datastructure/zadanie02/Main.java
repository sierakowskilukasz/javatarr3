package com.sda.datastructure.zadanie02;

import java.util.ArrayList;
import java.util.Random;

/*
Dodaj 10 losowych liczb do kolekcji.
Korzystając z indeksów pobierz pokolei wszystkie elementy i wyświetl je.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> liczbyLosowe = new ArrayList<>();
        Random randomGenerator = new Random();
        // Losowanie 10 liczb
        for (int i = 0; i < 10; i++) {
            liczbyLosowe.add(randomGenerator.nextInt(100)); // losuje w przedziale <0,99> lub <0,100)
        }
        // Wyświetlamy
        for (int i = 0; i < liczbyLosowe.size(); i++) {
            System.out.println(liczbyLosowe.get(i));
        }
    }
}
