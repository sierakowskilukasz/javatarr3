package com.sda.datastructure.zadanie01;

import java.util.TreeSet;

/*
Dodaj 5 imion do kolekcji. Wybierz tą kolekcję, która posortuje wprowadzone wartości.
Po dodaniu elementów wyświetl wszystkie
 */
public class Main {
    public static void main(String[] args) {
        TreeSet<String> listaImion = new TreeSet<>();
        listaImion.add("Jan");
        listaImion.add("Kamil");
        listaImion.add("Michalina");
        listaImion.add("Natalia");
        listaImion.add("Stanisław");
        listaImion.add("Łukasz");
        System.out.println(listaImion);
        for (String imie : listaImion) {
            System.out.println(imie);
        }
    }
}
