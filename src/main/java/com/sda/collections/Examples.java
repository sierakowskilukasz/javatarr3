package com.sda.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class Examples {
    public static void main(String[] args) {
        // Tworzenie listy
        List<String> listaString = new ArrayList<String>();
        // Dodawanie elementów
        listaString.add("Łukasz");
        listaString.add("Java");
        // Pobieranie elementów
        listaString.get(0); // Zwróci Łukasz
        listaString.get(1); // Zwróci Java
        listaString.get(2); // Zwróci IndexOutOfBoundsException
        listaString.add(0, "C#"); // Dodaje na zerowej pozycji
        listaString.get(0); // Zwróci C#
        String imie = listaString.get(1); // Zwróci Łukasz
        listaString.get(2); // Zwróci Java
        listaString.get(3); // Zwróci IndexOutOfBoundsException
        // Sprawdzanie ilości elementów w liście
        int iloscElementowWLiscie = listaString.size();
        // Wyświetlenie wszystkich elementów z listy
        for (int i = 0; i < listaString.size(); i++) {
            System.out.println(listaString.get(i));
        }

        // listaString.forEach(element -> System.out.println(element));

        // Pobranie ostatniego elementu z listy
        listaString.get(listaString.size() - 1);

        // Używanie HashSet
        HashSet<String> worekMikolaja = new HashSet<String>();
        boolean czyDodano = worekMikolaja.add("Java");// czyDodano ma wartość true
        czyDodano = worekMikolaja.add("Java"); // czyDodano ma wartość false
        // Usunięcie wszystkich elementów z HashSet
        worekMikolaja.clear();
        // Ilość elementów znajdująca się w HashSet (worekMikolaja)
        worekMikolaja.size();
        // Usuwanie elementu
        worekMikolaja.remove("Java");

        for (String elementWKolekcji : worekMikolaja) {
            System.out.println(elementWKolekcji);
        }

        // Używanie HashMap
        HashMap<Integer, String> mapaTekstowa = new HashMap<>();
        // Dodanie elementu do mapy
        mapaTekstowa.put(5, "pięć");
        mapaTekstowa.put(10, "dziesięć");
        mapaTekstowa.put(11, "jedenaście");
        // Pobranie elementu z mapy
        String wartosc = mapaTekstowa.get(5);
        System.out.println(wartosc);// Na konsoli pojawi się napis "pięć"
        // Sprawdzenie ilości elementów w mapie
        int iloscElementow = mapaTekstowa.size();
        // Wyświetlenie wszystkich kluczy znajdujących się mapie

        for (Integer klucz : mapaTekstowa.keySet()) {
            System.out.print(klucz + " ");
        } //Wynik działania powyższego for: 5 10 11

        for (Map.Entry<Integer, String> elementMapy : mapaTekstowa.entrySet()) {
            System.out.println("Klucz: " + elementMapy.getKey() + "Wartość: "
                               + elementMapy.getValue());
        }
        // Sprawdzenie czy klucz znajduje się już w mapie
        boolean czyIstniejeKlucz = mapaTekstowa.containsKey(5); // true

        // Czy podana wartość ("pięć") znajduje się już w mapie "mapaTekstowa"
        boolean czyInstniejeWartosc = mapaTekstowa.containsValue("pięć"); //true

        // Korzystanie z kolejki
        PriorityQueue<String> kolejka = new PriorityQueue<String>();
        // Dodanie elementu do kolejki
        kolejka.add("Java");
        kolejka.add("C#");
        kolejka.add("Python");

        // Pobranie elementu z kolejki
        // Pobranie elementu z kolejki bez usunięcia elementu z kolejki
        String elementZkolejki1 = kolejka.peek(); // Pobrałem "Java" i kolejka ma 3 elementy
        // Pobranie i usunięcie elementu z kolejki
        String elementZkolejki2 = kolejka.poll(); // Pobrałem "Java" i kolejka ma 2 elementy
        // Sprawdzenie ilości elementów w kolejce
        int iloscElementowWKolejce = kolejka.size();
    }
}
