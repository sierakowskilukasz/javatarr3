package com.sda;

import java.util.Scanner;

public class Utils {
    public static int pobierzLiczbeZKonsoli(){
        // Deklaracja i inicjalizacja klasy Scanner
        Scanner scanner = new Scanner(System.in);
        // Wyświetlanie komunikatu na konsoli
        System.out.print("Podaj liczbę: ");
        // Pobieranie liczby (int) z konsoli i przypisanie do
        // zmiennej wprowadzonaLiczba
        int wprowadzonaLiczba = scanner.nextInt();
        // Zwracamy zmienną wprowadzonaLiczba
        return wprowadzonaLiczba;
    }
}
