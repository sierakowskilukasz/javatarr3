package com.sda.calc;

public class Calculator {

    public int sumValues(int value1, int value2) {
        return value1 + value2;
    }

    public float divideValues(int value1, int value2){
        return value1 /(float)value2;
    }
}
