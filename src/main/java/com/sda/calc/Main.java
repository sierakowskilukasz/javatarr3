package com.sda.calc;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Pobieranie danych z konsoli od użytkownika
        Scanner skaner = new Scanner(System.in);
        System.out.println("Program wykonuje wskazaną operację dla 2 podanych liczb");
        System.out.print("Podaj 1 liczbę: ");
        int liczba1 = skaner.nextInt();
        System.out.print("Podaj 2 liczbę: ");
        int liczba2 = skaner.nextInt();
        System.out.print("Podaj operację (+-*/): ");
        skaner.nextLine();
        String operacja = skaner.nextLine();

        Float wynikOperacji = null;
        if (liczba1 >= 0 && liczba2 >= 0) {
            // Sprawdzamy jaką operację chce wykonać użytkownik
            // case sprawdza co wykonał użytkownik
            switch (operacja.charAt(0)) {
            case '*':
                wynikOperacji = (float) liczba1 * liczba2;
                break;
            case '+':
                wynikOperacji = (float) liczba1 + liczba2;
                break;
            case '-':
                wynikOperacji = (float) liczba1 - liczba2;
                break;
            case '/':
                if (liczba2 == 0) {
                    System.out.println("Nie można dzielić przez 0");
                } else {
                    wynikOperacji = (float) liczba1 / liczba2;
                }
                break;
            default:
                System.out.println("Nie rozpoznaję operacji");
            }
            //int suma = liczba1 + liczba2;
            if (wynikOperacji != null) {
                System.out.println("Wynik operacji wynosi: " + wynikOperacji);
            }
        } else {
            System.out.println("Podałeś liczbę ujemną. Nie umiem sumować takich liczb ;)");
        }
    }
}
