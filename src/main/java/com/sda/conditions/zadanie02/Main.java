package com.sda.conditions.zadanie02;

import com.sda.Utils;

/*
Przygotuj program, który pobierze od użytkownika 2 liczby i sprawdzi, która liczba
jest większa. Program powinien na zakończenie działania wyświetlić informację o
liczbie, która jest największa.
Przykładowe dane wejściowe: 2 i 8
Wynik działania programu: ​ “Liczba 8 jest większa od 2”
 */
public class Main {
    public static void main(String[] args) {
        // Pobieranie liczb z konsoli
        int liczba1 = Utils.pobierzLiczbeZKonsoli();
        int liczba2 = Utils.pobierzLiczbeZKonsoli();
        // Sprawdzanie, która liczba jest większa
        if (liczba1 > liczba2) {
            System.out.println("Liczba " + liczba1
                               + " jest większa od liczby" + liczba2);
        } else if (liczba1 < liczba2) {
            System.out.println("Liczba " + liczba1
                               + " jest mniejsza od liczby" + liczba2);
        } else {
            System.out.println("Liczba " + liczba1
                               + " jest równa liczbie" + liczba2);
        }
    }
}







