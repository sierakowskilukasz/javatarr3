package com.sda.designpatterns.strategy;

import java.security.NoSuchAlgorithmException;

public class PasswordHashGenerator {

    HashGenerator hashGenerator;

    public void setHashGenerator(HashGenerator hashGenerator) {
        this.hashGenerator = hashGenerator;
    }

    public String generate(String password) throws NoSuchAlgorithmException {
        return this.hashGenerator.generateHash(password);
    }
}
