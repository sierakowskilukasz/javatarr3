package com.sda.designpatterns.strategy;

import com.sda.designpatterns.strategy.PasswordHashGenerator;
import com.sda.designpatterns.strategy.hashgenerators.Md5Generator;
import com.sda.designpatterns.strategy.hashgenerators.Sha1Generator;

import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        String password = "Tajne hasłoddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
        PasswordHashGenerator passwordHashGenerator = new PasswordHashGenerator();

        passwordHashGenerator.setHashGenerator(new Sha1Generator());
        String sha1Password = passwordHashGenerator.generate(password);
        System.out.println(sha1Password);

        passwordHashGenerator.setHashGenerator(new Md5Generator());
        String md5Password = passwordHashGenerator.generate(password);
        System.out.println(md5Password);
    }
}
