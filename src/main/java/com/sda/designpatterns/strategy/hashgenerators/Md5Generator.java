package com.sda.designpatterns.strategy.hashgenerators;

import com.sda.designpatterns.strategy.HashGenerator;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Generator implements HashGenerator {

    @Override
    public String generateHash(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        String hash = DatatypeConverter.printHexBinary(digest);
        return hash;
    }
}
