package com.sda.designpatterns.strategy;

import java.security.NoSuchAlgorithmException;

public interface HashGenerator {
    public String generateHash(String password) throws NoSuchAlgorithmException;
}
