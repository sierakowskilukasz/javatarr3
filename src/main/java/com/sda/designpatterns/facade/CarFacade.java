package com.sda.designpatterns.facade;

public class CarFacade {

    public boolean startEngine(Car car) {
        boolean result = false;
        boolean engineState = car.checkEngine();
        boolean fuelState = car.checkFuelLevel();
        boolean oilLevel = car.checkOil();
        if (oilLevel == true && fuelState && engineState) {
            result = car.startEngine();
        }
        return result;
    }
}
