package com.sda.designpatterns.facade;

public class Car {
    public boolean checkEngine() {
        return true;
    }

    public boolean checkOil() {
        return true;
    }

    public boolean checkFuelLevel() {
        return true;
    }

    public boolean startEngine() {
        return true;
    }
}
