package com.sda.designpatterns.facade;

public class Main {
    private static CarFacade carFacade = new CarFacade();

    public static void main(String[] args) {
        Car car = new Car();
        carFacade.startEngine(car);
    }
}
