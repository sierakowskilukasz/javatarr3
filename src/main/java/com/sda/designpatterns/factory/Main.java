package com.sda.designpatterns.factory;

import com.sda.designpatterns.factory.reportcreator.DraftReportCreator;
import com.sda.designpatterns.factory.reportcreator.OfficialReportCreator;
import com.sda.designpatterns.factory.reportcreator.ReportCreator;
import com.sda.designpatterns.factory.reportcreator.UnofficialReportCreator;
import com.sda.designpatterns.factory.reports.Report;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<ReportCreator> reportCreators = Arrays.asList(new DraftReportCreator(),
                new OfficialReportCreator(), new UnofficialReportCreator());

        reportCreators.forEach(reportCreator -> {
            Report report = reportCreator.createReport();
            report.printReport();
        });
    }
}
