package com.sda.designpatterns.factory.reportcreator;


import com.sda.designpatterns.factory.reports.DraftReport;
import com.sda.designpatterns.factory.reports.Report;

public class DraftReportCreator implements ReportCreator {
    public Report createReport() {
        return new DraftReport();
    }
}
