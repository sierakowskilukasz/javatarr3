package com.sda.designpatterns.factory.reportcreator;


import com.sda.designpatterns.factory.reports.Report;

public interface ReportCreator {
    Report createReport();
}
