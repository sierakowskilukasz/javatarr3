package com.sda.designpatterns.factory.reportcreator;

import com.sda.designpatterns.factory.reports.OfficialReport;
import com.sda.designpatterns.factory.reports.Report;

public class OfficialReportCreator implements ReportCreator {
    public Report createReport() {
        return new OfficialReport();
    }
}
