package com.sda.designpatterns.factory.reportcreator;


import com.sda.designpatterns.factory.reports.Report;
import com.sda.designpatterns.factory.reports.UnofficialReport;

public class UnofficialReportCreator implements ReportCreator {
    @Override
    public Report createReport() {

        return new UnofficialReport();
    }
}
