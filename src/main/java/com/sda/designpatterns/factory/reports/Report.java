package com.sda.designpatterns.factory.reports;

public abstract class Report {
    public abstract void printReport();
}
