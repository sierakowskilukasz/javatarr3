package com.sda.designpatterns.factory.reports;

public class UnofficialReport extends Report {
    @Override
    public void printReport() {
        System.out.println("Raport nieoficjalny");
    }
}
