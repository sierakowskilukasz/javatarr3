package com.sda.designpatterns.adapter;


import com.sda.designpatterns.adapter.connector.Sms;

import java.util.Date;

public class SmsAdapter implements ReserveTable {
    Sms sms = new Sms();
    String telephoneNumber;

    @Override
    public boolean reserve(String person, Date date, int numberOfPersons) {

        if (sms.checkLine()) {
            String message = "Rezerwacja stolika na " + numberOfPersons + " osób na "
                    + person + "na dzień " + date;
            sms.sendSms(message, telephoneNumber);
        }
        return false;
    }
}
