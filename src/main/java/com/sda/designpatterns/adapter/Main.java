package com.sda.designpatterns.adapter;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        // Rezerwacja wysłana za pomocą E-maila
        ReserveTable reserveTable1 = new EmailAdapter();
        reserveTable1.reserve("Jan Kowalski", new Date(), 16);

        // Rezerwacja wysłana za pomocą SMS-a
        ReserveTable reserveTable2 = new SmsAdapter();
        reserveTable2.reserve("Jan Kowalski", new Date(), 16);
    }
}
