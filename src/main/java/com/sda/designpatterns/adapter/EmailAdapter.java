package com.sda.designpatterns.adapter;

import com.sda.designpatterns.adapter.connector.Email;

import java.util.Date;

public class EmailAdapter implements ReserveTable {

    Email email = new Email();
    String login = "login";
    String password = "password";
    private String emailAddress = "restauracja@gmai.com";

    @Override
    public boolean reserve(String person, Date date, int numberOfPersons) {

        email.connect();
        email.authorize(login, password);
        email.sendMessage("Rezerwacja stolika na " + numberOfPersons + " osób na "
                + person + "na dzień " + date);
        email.disconnect();
        return true;
    }
}
