package com.sda.designpatterns.adapter.connector;

public class Sms {
    public boolean checkLine() {
        return true;
    }

    public boolean sendSms(String message, String telephone) {
        return true;
    }
}
