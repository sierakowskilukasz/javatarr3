package com.sda.designpatterns.adapter.connector;

public class Email {

    public boolean connect() {
        return true;
    }

    public boolean authorize(String login, String pasword) {
        return true;
    }

    public boolean sendMessage(String message) {
        return true;
    }

    public boolean disconnect() {
        return true;
    }
}
