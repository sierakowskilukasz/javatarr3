package com.sda.oop.example.vehicle;

import com.sda.oop.example.engines.Engine;

import lombok.Getter;
import lombok.ToString;

/**
 * This is a base class for all cars
 */
@ToString
public class Car {

    private Engine engine;

    @Getter
    private boolean engineRunning;
    @Getter
    private String colour;
    @Getter
    protected int numberOfSeats = 5;

    /**
     * Creates instance of object {@link Car}
     *
     * @param colour Colour of the car
     */
    public Car(String colour, Engine engine) {
        this.colour = colour;
        this.engine = engine;
    }

    /**
     * Starts car engine
     */
    public void startEngine() {
        System.out.println("Uruchamianie silnika samochodu");
        engine.startEngine();
        engineRunning = true;
    }

    /**
     * Stops car engine
     */
    public void stopEngine() {
        System.out.println("Zatrzymywanie silnika samochodu");
        engineRunning = false;
    }
}
