package com.sda.oop.example.vehicle;

import com.sda.oop.example.engines.Engine;

public class SportCar extends Car {
    public SportCar(String colour, Engine engine) {
        super(colour, engine);
        numberOfSeats = 2;
    }

    /**
     * Add nitro to engine to increase horse power.
     */
    public void addNitroToEngine() {
        System.out.println("Wstrzyknięto dawkę nitro do silnika");
    }

    @Override
    public void startEngine() {
        System.out.println("Sprawdzenia ciśnienia w silniku");
        super.startEngine();
    }
}
