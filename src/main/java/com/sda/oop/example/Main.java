package com.sda.oop.example;

import java.util.ArrayList;
import java.util.List;

import com.sda.oop.example.engines.DieselEngine;
import com.sda.oop.example.engines.ElectricEngine;
import com.sda.oop.example.engines.Engine;
import com.sda.oop.example.engines.PetrolEngine;
import com.sda.oop.example.engines.TurboPetrolEngine;
import com.sda.oop.example.vehicle.Car;
import com.sda.oop.example.vehicle.SportCar;
import com.sda.oop.example.vehicle.SuperSportCar;

public class Main {
    public static void main(String[] args) {
        // Deklaracja i inicjalizacja klasy Car
        PetrolEngine petrolEngine = new PetrolEngine();
        Car samochod = new Car("czerwony", petrolEngine);
        // Uruchamiamy silnik
        samochod.startEngine();
        System.out.println(samochod.isEngineRunning());
        // Zatrzymujemy silnik
        samochod.stopEngine();
        System.out.println(samochod.isEngineRunning());
        System.out.println(samochod);

        Engine electricEngine = new ElectricEngine();
        SportCar samochodSportowy = new SportCar("czwerowny", electricEngine);
        Car samochodSportowy1 = new SportCar("czwerowny", petrolEngine);

        samochod.startEngine();
        samochodSportowy.startEngine();
        samochodSportowy1.startEngine();
        List<String> lista = new ArrayList<>();

        System.out.println("******** DEMONTSRACJA DZIEDZICZENIA ***********");
        Car samochod1 = new Car("czerwony", new DieselEngine());
        SportCar samochod2 = new SportCar("czarny", new PetrolEngine());
        SuperSportCar samochod3 = new SuperSportCar("biały", new ElectricEngine());
        Car samochod4 = new SuperSportCar("zielony", new TurboPetrolEngine());

        List<Car> samochody = new ArrayList<>();
        samochody.add(samochod1);
        samochody.add(samochod2);
        samochody.add(samochod3);
        samochody.add(samochod4);
        for (Car car : samochody) {
            System.out.println("Uruchamianie samochodu");
            car.startEngine();
        }
    }
}
