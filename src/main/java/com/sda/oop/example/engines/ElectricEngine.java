package com.sda.oop.example.engines;

public class ElectricEngine extends Engine{
    @Override
    public void startEngine() {
        System.out.println("Uruchamianie silnika elektrycznego");
    }

    @Override
    public int getOilLevel() {
        return 50;
    }

    @Override
    public int getCoolingFluidLevel() {
        return 50;
    }
}
