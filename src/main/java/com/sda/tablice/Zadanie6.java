package com.sda.tablice;

public class Zadanie6 {
    public static void main(String[] args) {
        int[] tablica = new int[] { 11, 55, 33, 22, 9817 };
        new Zadanie6().printArray(tablica);
    }

    public void printArray(int[] array) {
        System.out.print("{ ");
        // Pętla wykona się 5 razy dla i=0,1,2,3,4. Liczba 5 nie spełnia warunku
        // bo array.length ma wartość 5
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if (i != array.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.print(" }");
    }
}
