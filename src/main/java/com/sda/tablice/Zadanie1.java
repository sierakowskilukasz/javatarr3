package com.sda.tablice;

import java.util.Arrays;

public class Zadanie1 {
    public static void main(String[] args) {
        // Deklaracja 10 elementowej tablicy typu int
        int[] tablica = new int[10];
        tablica[0] = 33;
        tablica[1] = 2;
        tablica[2] = 1;
        tablica[3] = 44;
        tablica[4] = 1234;
        tablica[5] = 342;
        tablica[6] = 4657;
        tablica[7] = 43;
        tablica[8] = 4789;
        tablica[9] = 11122;
        String tablicaWPostaciTekstowej = Arrays.toString(tablica);
        System.out.println(tablicaWPostaciTekstowej);
        //System.out.println(Arrays.toString(tablica));
    }
}
