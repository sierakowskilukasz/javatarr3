package com.sda.tablice.Zadanie08;

public class OperacjeNaTablicy {

    /**
     * Metoda sprawdza czy w przekazanej tablicy znajduje się szukana wartość
     *
     * @param tablica            Tablica, w której ma być dokonane szukanie
     * @param wartoscPoszukiwana Wartość, która ma być wyszukana w tablicy.
     * @return Zwraca true, jeśli element został znaleziony. W przeciwnym wypadku zwraca false
     */
    public boolean contains(int[] tablica, int wartoscPoszukiwana) {
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == wartoscPoszukiwana) {
                return true;
            }
        }
        return false;
    }
}
