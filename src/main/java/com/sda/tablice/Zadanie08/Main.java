package com.sda.tablice.Zadanie08;

public class Main {
    public static void main(String[] args) {
        int[] tablica = new int[] { 1, 4, 33, 22, 64654, 9 };
        int wartoscPoszukiwana = 22;

        OperacjeNaTablicy operacjeNaTablicy = new OperacjeNaTablicy();
        boolean czyZnaleziono = operacjeNaTablicy.contains(tablica, wartoscPoszukiwana);

        // if (czyZnaleziono) {
        if (czyZnaleziono == true) {
            System.out.println("Liczba " + wartoscPoszukiwana
                               + " znajduje się w tablicy");
        } else {
            System.out.println("Liczba " + wartoscPoszukiwana
                               + " nie znajduje się w tablicy");
        }
    }
}
