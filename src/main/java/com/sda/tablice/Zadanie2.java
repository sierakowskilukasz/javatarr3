package com.sda.tablice;

import java.util.Arrays;

public class Zadanie2 {
    public static void main(String[] args) {
        float[] tablica = new float[5];
        tablica[0] = 4.5f;
        tablica[1] = (float)4.6;
        tablica[2] = 4;
        tablica[3] = 13.5678f;
        tablica[4] = 111.111f;

        String tablicaWPosaciTekstowej = Arrays.toString(tablica);
        System.out.println(tablicaWPosaciTekstowej);
//        System.out.println(Arrays.toString(tablica));
    }
}
