package com.sda.regularexpressions.zadanie04;

import java.util.regex.Pattern;

import com.sda.Utils;

/*
   Napisz aplikację, która sprawdza, czy wprowadzona przez użytkownika liczba jest 3 cyfrowa
 */
public class Main {
    public static void main(String[] args) {
        int liczba = Utils.pobierzLiczbeZKonsoli();
        boolean jestOk = Pattern.matches("^[0-9]{3}$", Integer.toString(liczba));
        if (jestOk) {
            System.out.println("Liczba jest poprawna");
        } else {
            System.out.println("Liczbe jest niepoprawna");
        }
    }
}
