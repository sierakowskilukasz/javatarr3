package com.sda.regularexpressions.zadanie01;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków
zawiera tylko i wyłącznie duże litery.
 */
public class Main {
    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        System.out.print("Wprowadź tekst: ");
        String tekst = skaner.nextLine();

        Pattern wzorzec = Pattern.compile("[A-Z]+");
        Matcher porownywacz = wzorzec.matcher(tekst);
        if (porownywacz.matches()) {
            System.out.println("Wprowadzony tekst zawiera tylko duże litery");
        } else {
            System.out.println("Wprowadzony tekst jest niepoprawny");
        }
    }
}
