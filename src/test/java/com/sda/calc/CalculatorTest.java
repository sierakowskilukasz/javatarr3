package com.sda.calc;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

    @Test
    public void shouldSum2ValuesCorrectly() {
        Calculator calculator = new Calculator();
        int suma = calculator.sumValues(37, 52);
        Assert.assertTrue(suma == 89);
    }

    @Test
    public void shouldDivide2ValuesCorrectly() {
        Calculator calculator = new Calculator();
        float iloraz = calculator.divideValues(99, 11);
        Assert.assertTrue(iloraz == 9.0f);
    }
}